#include <iostream>
#include "CounterError.h"
#include <pcrecpp.h>

using namespace std;
using namespace pcrecpp;

CounterError::CounterError() {
}

CounterError::~CounterError() {
}

int CounterError::CountErorrs(){
		int counter = 0; 	
		
		//Регулярное выражение для поиска строк с ошибками
		RE re(".+error:.+");    
		
		//Чтение из файла и увеличение счетчика
		string line = ""; 	
				
		while( getline (cin,line) )
		{
			if(re.FullMatch(line))
				counter++;
		}

		return  counter;
}

