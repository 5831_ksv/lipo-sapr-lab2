CC=g++
PCREFLAG=-lpcrecpp
TOFILEFLAG=-o
MAININCLUDES=CounterError/CounterError.h CounterError/CounterError.cpp
MAINSRC=main.cpp
TESTINCLUDES=$(MAININCLUDES) CounterErrorTest/CounterErrorTester.h CounterErrorTest/CounterErrorTester.cpp
TESTSRC=CounterErrorTest/test_runner.cpp
MAINEXECUTABLE=errorCounter
TESTEXECUTABLE=test

ifndef FILETOCHECK
	FILETOCHECK=test_files/3_errors.log
endif

programResult=$(shell ./$(MAINEXECUTABLE) < $(FILETOCHECK))
scriptResult=$(shell bash $(MAINEXECUTABLE).sh < $(FILETOCHECK))

ifeq ($(programResult),$(scriptResult))
	compareResult=Results of the program and the script NOT equal
else
	compareResult=Results of the program and the script equal
endif

all: compile_program

compile_program:
	@$(CC) $(MAINSRC) $(MAININCLUDES) $(TOFILEFLAG) $(MAINEXECUTABLE) $(PCREFLAG) 

compile_tests:
	@$(CC) $(TESTSRC) $(TESTINCLUDES) $(TOFILEFLAG) $(TESTEXECUTABLE) $(PCREFLAG) 

test: compile_tests
	@./test
	@rm -f test

check:
	@printf '$(compareResult)\n'
	@printf '\tProgram: $(programResult)\n'
	@printf '\tScript: $(scriptResult)\n'

clean:
	@rm -f $(MAINEXECUTABLE) $(TESTEXECUTABLE)

