#include <fstream>
#include <iostream>
#include "CounterErrorTester.h"
#include "../CounterError/CounterError.h"

using namespace std;

CounterErrorTester::CounterErrorTester() {
}

CounterErrorTester::~CounterErrorTester() {
}

int CounterErrorTester::testErrorCounter(ifstream& aInStream, CounterError& bProcessor, const char* cFilename)
{	
	//открытие потока из файла
	aInStream.open(cFilename);		
	cin.rdbuf(aInStream.rdbuf());
	
	//вызов счетчика ошибок
	int result = bProcessor.CountErorrs();  
	
	//закрытие потока
	aInStream.close();			
	return result;				
}						

