#include "../CounterError/CounterError.h"
#include <iostream>
#include <fstream>
#include "CounterErrorTester.h"

using namespace std;

int main()
{
	CounterError processor;
	CounterErrorTester processorTester;
	ifstream inStream;			

	//проведение теста в специальных файлах с определенным фиксированным количеством ошибок
	int test1 = processorTester.testErrorCounter(inStream, processor, "test_files/3_errors.log");
	int test2 = processorTester.testErrorCounter(inStream, processor, "test_files/4_errors.log");
	int test3 = processorTester.testErrorCounter(inStream, processor, "test_files/8_errors.log");

	if(test1 == 3) 
		cout << "Test 1. TestFile include 3 errors: OK" << endl;  
	else
		cout << "Test 1: FAIL\n\tОжидаемый результат: 3\n\tПолученный результат: " << test1 << endl;

	if(test2 == 4)
		cout << "Test 2. TestFile include 4 errors: OK" << endl;
	else
		cout << "Test 2: FAIL\n\tОжидаемый результат: 4\n\tПолученный результат: " << test2 << endl;

	if(test3 == 8)
		cout << "Test 3. TestFile include 8 errors: OK" << endl;
	else
		cout << "Test 3: FAIL\n\tОжидаемый результат: 8\n\tПолученный результат: " << test3 << endl;

	return 0;
}
